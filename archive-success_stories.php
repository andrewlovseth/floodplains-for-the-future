<?php get_header(); ?>

    <?php get_template_part('templates/archive-success-stories/page-title'); ?>

    <?php get_template_part('templates/archive-success-stories/post-list'); ?>

<?php get_footer(); ?>