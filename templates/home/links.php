<section class="links grid">
    <div class="link-grid">

        <?php if(have_rows('links')): while(have_rows('links')): the_row(); ?>

            <?php
                $link = get_sub_field('link');
                $icon = get_sub_field('icon');
                $dek = get_sub_field('dek');
                if( $link ): 
                $link_url = $link['url'];
                $link_title = $link['title'];
                $link_target = $link['target'] ? $link['target'] : '_self';
            ?>

                <div class="link-item">
                    <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
                        <div class="icon">
                            <?php echo wp_get_attachment_image($icon['ID'], 'full'); ?>
                        </div>

                        <div class="info">
                            <div class="headline">
                                <h3><?php echo esc_html($link_title); ?></h3>
                            </div>

                            <div class="dek copy copy-2">
                                <p><?php echo $dek; ?></p>
                            </div>
                        </div>                        
                    </a>
                </div>

            <?php endif; ?>
            
        <?php endwhile; endif; ?>

    </div>
</section>