<?php

    $about = get_field('about');
    $about_copy = $about['copy'];

    $whats_new = get_field('whats_new');
    $whats_new_headline = $whats_new['headline'];
    $whats_new_copy = $whats_new['copy'];

?>

<section class="about grid">
    <div class="about-copy copy copy-1 extended">
        <?php echo $about_copy; ?>
    </div>

    <div class="card green whats-new">
        <div class="card-header headline">
            <h2><?php echo $whats_new_headline; ?></h2>
        </div>

        <div class="card-body copy copy-1 extended">
            <?php echo $whats_new_copy; ?>
        </div>
    </div>
</section>

