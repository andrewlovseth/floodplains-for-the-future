<section class="hero">
	<div class="swiper">
		<div class="swiper-wrapper">

			<?php if(have_rows('hero')): while(have_rows('hero')): the_row(); ?>

				<div class="swiper-slide hero-slide">
					<div class="photo">
						<?php $image = get_sub_field('photo'); if( $image ): ?>
							<?php echo wp_get_attachment_image($image['ID'], 'full'); ?>
						<?php endif; ?>
					</div>

					<div class="info">
						<div class="headline">
							<h1><?php echo get_sub_field('headline'); ?></h1>
						</div>

						<?php 
							$link = get_sub_field('cta');
							if( $link ): 
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
						?>

							<div class="cta">
								<a class="btn" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
							</div>

						<?php endif; ?>

					</div>
				</div>

			<?php endwhile; endif; ?>
		</div>

		<div class="swiper-pagination"></div>
		<div class="swiper-button-prev"></div>
		<div class="swiper-button-next"></div>
	</div>
</section>