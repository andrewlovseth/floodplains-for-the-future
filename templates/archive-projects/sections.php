<?php
    $projects = get_field('projects', 'options');
    $categories = $projects['categories'];
    if($categories):
?>

    <?php foreach($categories as $category): ?>
        
        <?php        
            $term = get_term( $category );
            $name = $term->name;
            $slug = $term->slug;

            $args = array(
                'post_type' => 'projects',
                'posts_per_page' => 100,
                'order' => 'ASC',
                'orderby' => 'title',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'status',
                        'field'    => 'term_id',
                        'terms'    => $category,
                    ),
                )
            );
            $query = new WP_Query( $args );
            if ( $query->have_posts() ) : ?>
            
                <section class="project-group <?php echo $slug; ?> grid">
                    <div class="section-header">
                        <h2 class="section-title"><?php echo $name; ?></h2>

                        <?php if($slug == 'conservation-easements'): ?>
                            <div class="copy copy-2 extended">
                                <?php echo get_field('projects_conservation_easements_copy', 'options'); ?>
                            </div>
                        <?php endif; ?>
                    </div>

                    <div class="project-grid">
                        <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                            <?php 
                                $external_link = get_field('link');
                                if($external_link) {
                                    $link = $external_link;
                                } else {
                                    $link = get_permalink();
                                }
                            ?>

                            <article class="project">
                                <div class="photo">
                                    <a href="<?php echo $link; ?>"<?php if($external_link): ?> target="window"<?php endif; ?>>
                                        <?php if(get_the_post_thumbnail()): ?>
                                            <?php the_post_thumbnail('medium'); ?>
                                        <?php else: ?>
                                            <img src="<?php bloginfo('template_directory'); ?>/src/images/FPO-project.jpeg" alt="FPO" />
                                        <?php endif; ?>
                                    </a>
                                </div>

                                <div class="info">
                                    <h3><a href="<?php echo $link; ?>"><?php the_title(); ?></a></h3>
                                </div>                                
                            </article>
                        
                        <?php endwhile; ?>
                    </div>
                </section>

        <?php endif; wp_reset_postdata(); ?>

    <?php endforeach; ?>

<?php endif; ?>