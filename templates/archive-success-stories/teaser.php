<article <?php post_class('teaser'); ?>>

    <div class="photo">
        <a href="<?php the_permalink(); ?>">
            <?php the_post_thumbnail('large'); ?>
        </a>
    </div>

    <div class="info">
        <?php if(get_field('date')): ?>
            <div class="date">
                <h4><?php echo get_field('date'); ?></h4>
            </div>

        <?php endif; ?>

        <div class="headline">
            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
        </div>

        <div class="copy copy-2">
            <?php the_excerpt(); ?>
        </div>

        <div class="cta">
            <a href="<?php the_permalink(); ?>" class="underline">Read More</a>
        </div>
    </div>

</article>