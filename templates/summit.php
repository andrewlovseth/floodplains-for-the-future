<?php

/*
	Template Name: Summit
*/

get_header(); ?>

	<?php get_template_part('template-parts/global/page-title'); ?>

	<?php get_template_part('templates/summit/intro'); ?>

    <?php get_template_part('templates/summit/work'); ?>


<?php get_footer(); ?>