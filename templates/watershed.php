<?php

/*
	Template Name: Watershed
*/

get_header(); ?>

	<?php get_template_part('template-parts/global/page-title'); ?>

  	<?php get_template_part('templates/watershed/intro'); ?>

  	<?php get_template_part('templates/watershed/map'); ?>


<?php get_footer(); ?>