<?php

/*
	Template Name: About
*/

get_header(); ?>


	<?php get_template_part('template-parts/global/page-title'); ?>

	<?php get_template_part('templates/about/page-header'); ?>

	<?php get_template_part('templates/about/vision-mission'); ?>

	<?php get_template_part('templates/about/goals-strategies'); ?>
	
	<?php get_template_part('templates/about/partners'); ?>
    
<?php get_footer(); ?>