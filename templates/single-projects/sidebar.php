<?php
    $page_title = get_the_title();
    $status = get_the_terms( $post->ID, 'status' );
    $primary_status = $status[0]->slug;
    $projects = get_field('projects', 'options');
    $categories = $projects['categories'];
    if($categories):
?>

    <aside class="projects-sidebar">

        <div class="sidebar-header">
            <h3>All Projects</h3>
        </div>

        <?php foreach($categories as $category): ?>
            
            <?php        
                $term = get_term( $category );
                $name = $term->name;
                $slug = $term->slug;

                $args = array(
                    'post_type' => 'projects',
                    'posts_per_page' => 100,
                    'order' => 'ASC',
                    'orderby' => 'title',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'status',
                            'field'    => 'term_id',
                            'terms'    => $category,
                        ),
                    )
                );
                $query = new WP_Query( $args );
                if ( $query->have_posts() ) : ?>

                <div class="project-group<?php if($primary_status === $slug): ?> active<?php endif; ?>">
                    <div class="header">
                        <h4><?php echo $name; ?></h4>
                    </div>
                    
                    <div class="copy copy-3">
                        <ul>
                            <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                                <?php 
                                    $external_link = get_field('link');
                                    $link_title = get_the_title();
                                    if($external_link) {
                                        $link_url = $external_link;
                                    } else {
                                        $link_url = get_permalink();
                                    }

                                    $link_class = "project-link";
                                    if($page_title === $link_title) {
                                        $link_class .= " current";
                                    }
                                ?>
                                <li><a href="<?php echo $link_url; ?>" class="<?php echo $link_class; ?>"><?php echo $link_title; ?></a></li>
                            <?php endwhile; ?>
                        </ul>
                    </div>
                </div>

            <?php endif; wp_reset_postdata(); ?>

        <?php endforeach; ?>

    </aside>

<?php endif; ?>