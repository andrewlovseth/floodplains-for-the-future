<?php
    $args = wp_parse_args($args);
    
    if(!empty($args)) {
        $resource = $args['resource']; 
    }

    $type = get_field('type', $resource->ID);
    $file = get_field('file', $resource->ID);
    $link = get_field('link', $resource->ID);

    if($type == 'fftf-document' || $type == 'other-document' ) {
        $link_url = $file['url'];

    } else if($type == 'external-link') {
        $link_url = $link;
    }

    $link_title = get_the_title($resource->ID);
?>

<a class="btn" href="<?php echo esc_url($link_url); ?>" target="window"><?php echo esc_html($link_title); ?></a>