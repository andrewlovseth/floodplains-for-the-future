<?php

    $intro = get_field('intro');
    $headline = $intro['headline'];
    $copy = $intro['copy']; 
    $photo = $intro['photo'];

?>

<section class="intro grid">

    <div class="photo">
        <?php echo wp_get_attachment_image($photo['ID'], 'full'); ?>
    </div>

    <div class="info">
        <div class="headline">
            <h2><?php echo $headline; ?></h2>
        </div>

        <div class="copy copy-2">
            <?php echo $copy; ?>
        </div>
    </div>

</section>