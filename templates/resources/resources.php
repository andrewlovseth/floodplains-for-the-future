<?php if(have_rows('resources')): ?>

    <section class="resources-list grid">
        <div class="resources-grid">

            <?php while(have_rows('resources')) : the_row(); ?>

                <?php if( get_row_layout() == 'section' ): ?>
                    <?php
                        $headline = get_sub_field('headline'); 
                        $copy = get_sub_field('copy'); 
                        $color = get_sub_field('color'); 
                        $resources = get_sub_field('resources'); 
                    ?>

                    <div class="resources-card card <?php echo $color; ?>">
                        <div class="card-header">
                            <div class="headline">
                                <h3><?php echo $headline; ?></h3>
                            </div>
                        </div>

                        <div class="card-body">
                            <?php if($copy): ?>
                                <div class="description copy copy-2">
                                    <?php echo $copy; ?>
                                </div>
                            <?php endif; ?>

                            <?php if( $resources ): ?>
                                <ul class="link-list copy copy-3">
                                    <?php foreach( $resources as $resource ): ?>
                                        <li>

                                            <?php
                                                $args = ['resource' => $resource];
                                                get_template_part('templates/resources/resource-type', null, $args);
                                            ?>      

                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>                                
                        </div>
                    </div>

                <?php endif; ?>

            <?php endwhile; ?>
            
        </div>
    </section>

<?php endif; ?>