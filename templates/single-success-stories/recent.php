<section class="recent grid">

    <div class="section-header">
        <h2 class="section-title">Recent Success Stories</h2>
    </div>

    <div class="post-grid">
        <?php
            $currentPost = get_the_ID();
            $args = array(
                'post_type' => 'success_stories',
                'posts_per_page' => 3,
                'post__not_in' => array($currentPost),
            );
            $query = new WP_Query( $args );
            if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

            <?php get_template_part('templates/single-success-stories/recent-teaser'); ?>

        <?php endwhile; endif; wp_reset_postdata(); ?>
    </div>

</section>