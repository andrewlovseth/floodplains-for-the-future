<article <?php post_class('grid'); ?>>
    <section class="page-header">            
        <div class="meta">
            <a href="<?php echo site_url('/success-stories/'); ?>" class="section">Success Stories</a>


            <?php if(get_field('date')): ?>
                <span class="divider">|</span>
                <span class="date"><?php echo get_field('date'); ?></span>
            <?php endif; ?>
        </div>            

        <h1 class="page-title"><?php the_title(); ?></h1>
    </section>

    <div class="featured-photo">
        <?php the_post_thumbnail('large'); ?>
    </div>

    <div class="copy copy-1 extended">
        <?php the_content(); ?>
    </div>
</article>