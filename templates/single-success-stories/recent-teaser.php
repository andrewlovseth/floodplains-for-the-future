<article <?php post_class('recent-teaser'); ?>>

    <div class="photo">
        <a href="<?php the_permalink(); ?>">
            <?php the_post_thumbnail('medium'); ?>
        </a>
    </div>

    <div class="info">
        <div class="headline">
            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
        </div>
    </div>

</article>