<?php

    $headline = get_field('glossary_headline');

?>

<section class="glossary grid">
    <div class="section-header">
        <h2 class="section-title"><?php echo $headline; ?></h2>
    </div>

    <ul class="glossary-list">
        <?php if(have_rows('glossary')): $count = 1; while(have_rows('glossary')): the_row(); ?>
        
            <li class="entry<?php if($count === 1): ?> open<?php endif; ?>">
                <h3 class="word">
                    <?php echo get_sub_field('word'); ?>
                    <?php get_template_part('template-parts/svg/glossary-toggle'); ?>
                </h3>

                <div class="description copy copy-1">
                    <p><?php echo get_sub_field('description'); ?></p>
                </div>                
            </li>

        <?php $count++; endwhile; endif; ?>
    </ul>
</section>