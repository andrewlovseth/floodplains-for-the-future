<?php

    $intro = get_field('intro');
    $headline = $intro['headline'];
    $copy = $intro['copy'];
    $photo = $intro['photo'];

?>

<section class="intro grid">

    <div class="photo">
        <?php echo wp_get_attachment_image($photo['ID'], 'full'); ?>
    </div>

    <div class="card blue">
        <div class="card-header">
            <h3><?php echo $headline; ?></h3>
        </div>

        <div class="card-body copy copy-2 extended">
            <?php echo $copy; ?>
        </div>
    </div>

</section>