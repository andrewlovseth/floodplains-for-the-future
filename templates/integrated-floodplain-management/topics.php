<section class="topics">

    <?php if(have_rows('topics')): while(have_rows('topics')) : the_row(); ?>

        <?php if( get_row_layout() == 'topic' ): ?>
            
            <?php
                $title = get_sub_field('title');
                $slug = sanitize_title_with_dashes($title);
                $icon = get_sub_field('icon');
                $copy = get_sub_field('copy');
                $video = get_sub_field('video');
            ?>

            <div class="topic grid">
                <div class="section-header">
                    <div class="icon">
                        <?php echo wp_get_attachment_image($icon['ID'], 'full'); ?>
                    </div>

                    <h3><?php echo $title; ?></h3>
                </div>

                <div class="video">
                    <?php echo $video; ?>
                </div>		

                <div class="copy copy-1 extended">
                    <?php echo $copy; ?>
                </div>	
            </div>

        <?php endif; ?>

    <?php endwhile; endif; ?>

</section>