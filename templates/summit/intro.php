<?php

    $intro = get_field('intro');
    $copy = $intro['copy'];

?>

<section class="intro grid">


    <div class="copy copy-2 extended">
        <?php echo $copy; ?>
    </div>

</section>