<?php

    $reaches = get_the_terms( $post->ID, 'reach' ); 

    if($reaches) {
        $reach_filter = '';
        foreach($reaches as $reach) {
            $reach_filter .= $reach->slug;
        }
    } else {
        $reach_filter = '';
    }
?>

<div class="work-item | card" data-reach-filter="<?php echo $reach_filter; ?>">

    <div class="work-item__headline | card__header">
        <h3 class="work-item__title"><?php the_title(); ?></h3>
    </div>

    <div class="card__body">
        <div class="work-item__copy | copy copy-3 extended">
            <?php the_content(); ?>
        </div>
    </div>
</div>