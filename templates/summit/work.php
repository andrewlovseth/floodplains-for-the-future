<?php
    $work_cats = get_terms(
        array( 
            'taxonomy' => 'work-category',
            'hide_empty'   => true,
            'orderby' => 'slug',
        )
    );

    $reaches = get_terms(
        array( 
            'taxonomy' => 'reach',
            'hide_empty'   => true,
            'orderby' => 'slug',
        )
    );

    $sectionClass = 'work-tabs | tabs grid';

?>

<section class="<?php echo $sectionClass; ?>">

    <div class="section-header">
        <h2 class="section-title dark-blue">Work Portfolio</h2>
    </div>
        
    <div class="tab-list">        
        <?php $count = 1; foreach($work_cats as $work_cat): ?>
            <?php
                $title = $work_cat->name;
                $slug = $work_cat->slug;
            ?>

            <a href="#" <?php if($count === 1): ?>aria-selected="true" <?php endif; ?>role="tab" id="<?php echo $slug; ?>">
                <?php echo $title; ?>
            </a>
        <?php $count++; endforeach; ?>
    </div>  

    <?php $count = 1; foreach($work_cats as $work_cat): ?>

        <?php
            $title = $work_cat->name;
            $slug = $work_cat->slug;
        ?>

        <section class="group tab-panel <?php echo $slug; ?>" role="tabpanel"  aria-labelledby="<?php echo $slug; ?>"<?php if($count > 1): ?> hidden<?php endif; ?>>
            <div class="tab-header">
                <h3 class="tab-title"><?php echo $title; ?></h3>

                <div class="filter">
                    <label for="reach">Filter by Reach</label>
                    <select class="filter-select" name="reach">
                        <option value="all">All Reaches</option>
                        <?php foreach($reaches as $reach): ?>
                            <option value="<?php echo $reach->slug; ?>"><?php echo $reach->name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>

            <div class="work-grid">

                <?php
                    $args = array(
                        'post_type' => 'work',
                        'posts_per_page' => 100,
                        'tax_query' => array(
                            array (
                                'taxonomy' => 'work-category',
                                'field' => 'slug',
                                'terms' => $work_cat->slug,
                            )
                        ),
                    );
                    $query = new WP_Query( $args );
                    if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

                    <?php get_template_part('templates/summit/work-item'); ?>

                <?php endwhile; endif; wp_reset_postdata(); ?>

                <p class="no-results copy copy-2" style="display:none;">No filtered results for this reach. Choose a different reach.</p>

            </div>                
        </section>
    <?php $count++; endforeach; ?>

</section>