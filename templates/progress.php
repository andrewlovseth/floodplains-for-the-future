<?php

/*
	Template Name: Progress
*/

get_header(); ?>
	
	<?php get_template_part('templates/progress/page-header'); ?>

	<?php get_template_part('templates/progress/tabs'); ?>
    
<?php get_footer(); ?>