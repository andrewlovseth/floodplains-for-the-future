<?php

    $vision = get_field('vision');
    $vision_headline = $vision['headline'];
    $vision_copy = $vision['copy'];

    $mission = get_field('mission');
    $mission_headline = $mission['headline'];
    $mission_copy = $mission['copy'];

?>

<section class="vision-mission grid">
    <div class="vision">
        <div class="headline">
            <h4><?php echo $vision_headline; ?></h4>
        </div>

        <div class="copy copy-2">
            <?php echo $vision_copy; ?>
        </div>
    </div>

    <div class="mission">
        <div class="headline">
            <h4><?php echo $mission_headline; ?></h4>
        </div>

        <div class="copy copy-2">
            <?php echo $mission_copy; ?>
        </div>
    </div>

</section>