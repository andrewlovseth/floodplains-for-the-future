<?php

    $page_header = get_field('page_header');
    $video = $page_header['video'];
    $headline = $page_header['headline'];
    $copy = $page_header['copy'];

?>

<section class="about-page-header grid">    
    <?php if($video): ?>
        <div class="video">
            <?php echo $video; ?>
        </div>
    <?php endif; ?>

    <div class="info">
        <?php if($headline): ?>
            <div class="headline">
                <h3 class="section-title"><?php echo $headline; ?></h3>
            </div>
        <?php endif; ?>

        <?php if($copy): ?>
            <div class="copy copy-1">
                <?php echo $copy; ?></h3>
            </div>
        <?php endif; ?>
    </div>
</section>