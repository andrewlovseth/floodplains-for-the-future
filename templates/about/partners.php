<?php 

    $partners = get_field('partners');
    $headline = $partners['headline'];
    $background_image = $partners['background_image'];

if(have_rows('partners')): while(have_rows('partners')): the_row(); ?>

    <section class="partners grid" style="background-image: url(<?php echo $background_image['url']; ?>);">

        <div class="headline">
            <h3 class="section-title white align-center"><?php echo $headline; ?></h3>
        </div>

        <div class="list">
            <?php if(have_rows('list')): while(have_rows('list')): the_row(); ?>
            
                <?php 
                    $link = get_sub_field('link');
                    if( $link ): 
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
                ?>

                    
                    <div class="partner">
                        <?php if($link_url === "#"): ?>
                            <span class="partner-link"><?php echo esc_html($link_title); ?></span>
                        <?php else: ?>
                            <a class="partner-link" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                        <?php endif; ?>
                    </div>

                <?php endif; ?>

            <?php endwhile; endif; ?>
        </div>

    </section>

<?php endwhile; endif; ?>