<?php

    $goals = get_field('goals');
    $goals_headline = $goals['headline'];
    $goals_sub_header = $goals['sub_header'];
    $goals_copy = $goals['copy'];
    $goals_photo = $goals['photo'];

    $strategies = get_field('strategies');
    $strategies_headline = $strategies['headline'];
    $strategies_sub_header = $strategies['sub_header'];
    $strategies_copy = $strategies['copy'];
    $strategies_photo = $strategies['photo'];

?>

<section class="goals-strategies grid">
    <div class="goals content-section">
        <div class="photo">
            <?php echo wp_get_attachment_image($goals_photo['ID'], 'large'); ?>
        </div>

        <div class="info">
            <div class="headline">
                <h4><?php echo $goals_headline; ?></h4>
                <h5><?php echo $goals_sub_header; ?></h5>
            </div>

            <div class="copy copy-2">
                <?php echo $goals_copy; ?>
            </div>
        </div>

    </div>

    <div class="strategies content-section">
        <div class="photo">
            <?php echo wp_get_attachment_image($strategies_photo['ID'], 'large'); ?>
        </div>

        <div class="info">
            <div class="headline">
                <h4><?php echo $strategies_headline; ?></h4>
                <h5><?php echo $strategies_sub_header; ?></h5>
            </div>

            <div class="copy copy-2">
                <?php echo $strategies_copy; ?>
            </div>
        </div>
    </div>

</section>