<?php

    $map = get_field('map');
    $headline = $map['headline'];
    $copy = $map['copy'];
    $photo = $map['photo'];
    $link = $map['cta'];
    $link_url = $link['url'];
    $link_title = $link['title'];
    $link_target = $link['target'] ? $link['target'] : '_self';

    $how_headline = $map['how_headline'];
    $how_copy = $map['how_copy'];

?>

<section class="map grid">


    <div class="info">
        <div class="headline">
            <h3><?php echo $headline; ?></h3>
        </div>

        <div class="copy copy-1 extended">
            <?php echo $copy; ?>
        </div>

        <div class="cta">
            <a class="btn small" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
        </div>
    </div>


    <div class="how-to card card-small blue">
        <div class="card-header">
            <h3><?php echo $how_headline; ?></h3>
        </div>

        <div class="card-body copy copy-2 extended">
            <?php echo $how_copy; ?>
        </div>
    </div>

    <div class="photo">
        <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
            <?php echo wp_get_attachment_image($photo['ID'], 'full'); ?>
        </a>
    </div>

    


</section>