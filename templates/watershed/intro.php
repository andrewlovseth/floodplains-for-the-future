<?php

    $intro = get_field('intro');
    $headline = $intro['headline'];
    $copy = $intro['copy'];
    $photo = $intro['photo'];

?>

<section class="intro grid">

    <div class="photo">
        <?php echo wp_get_attachment_image($photo['ID'], 'full'); ?>
    </div>

    <div class="info">
        <div class="headline">
            <h3><?php echo $headline; ?></h3>
        </div>

        <div class="copy copy-1 extended">
            <?php echo $copy; ?>
        </div>
    </div>

</section>