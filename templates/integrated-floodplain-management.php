<?php

/*
	Template Name: Integrated Floodplain Management
*/

get_header(); ?>

	<?php get_template_part('template-parts/global/page-title'); ?>

   	<?php get_template_part('templates/integrated-floodplain-management/intro'); ?>

	<?php get_template_part('templates/integrated-floodplain-management/topics'); ?>

	<?php get_template_part('templates/integrated-floodplain-management/glossary'); ?>

<?php get_footer(); ?>