<?php

/*
	Template Name: Resources
*/

get_header(); ?>

	<?php get_template_part('template-parts/global/page-title'); ?>

	<?php get_template_part('templates/resources/intro'); ?>

	<?php get_template_part('templates/resources/resources'); ?>

<?php get_footer(); ?>