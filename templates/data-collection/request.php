<?php

    $request = get_field('request');
    $show = $request['show'];
    $headline = $request['headline'];
    $copy = $request['copy'];
    $icon = $request['icon'];
    $link = $request['cta'];

    if($show === TRUE): 

?>

    <section class="request grid">
        <div class="request-container">

            <div class="icon">
                <?php echo wp_get_attachment_image($icon['ID'], 'full'); ?>
            </div>

            <div class="info">
                <div class="headline">
                    <h3><?php echo $headline; ?></h3>
                </div>

                <div class="copy copy-2">
                    <?php echo $copy; ?>
                </div>

                <?php 
                    if( $link ): 
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
                ?>

                    <div class="cta">
                        <a class="btn white-outline" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                    </div>

                <?php endif; ?>
            </div>

        </div>
    </section>

<?php endif; ?>