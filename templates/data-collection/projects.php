<?php

    $tab_count = count(get_field('project_groups'));

    $sectionClass = 'tabs projects grid';
    if($tab_count <= 1) {
        $sectionClass .= ' no-tabs';
    }

?>

<section class="<?php echo $sectionClass; ?>">

    <?php if($tab_count > 1): ?>
        <?php if(have_rows('project_groups')): ?>
            <div class="tab-list">
                
                <?php $count = 1; while(have_rows('project_groups')) : the_row(); ?>
                    <?php if( get_row_layout() == 'group' ): ?>

                        <?php
                            $title = get_sub_field('group_title');
                            $slug = sanitize_title_with_dashes($title);
                        ?>

                        <a href="#" <?php if($count === 1): ?>aria-selected="true" <?php endif; ?>role="tab" id="<?php echo $slug; ?>"><?php echo $title; ?></a>
                        
                    <?php endif; ?>
                <?php $count++; endwhile; ?>

            </div>
        
        <?php endif; ?>
    <?php endif; ?>


    <?php if(have_rows('project_groups')): $count = 1; while(have_rows('project_groups')) : the_row(); ?>

        <?php if( get_row_layout() == 'group' ): ?>

            <?php
                $title = get_sub_field('group_title');
                $slug = sanitize_title_with_dashes($title);
            ?>

            <section class="group tab-panel <?php echo $slug; ?>" role="tabpanel" aria-labelledby="<?php echo $slug; ?>"<?php if($count > 1): ?> hidden<?php endif; ?>>
                <div class="section-header">
                    <h2 class="section-title"><?php echo $title; ?></h2>
                </div>

                <div class="project-grid">
                    <?php if(have_rows('projects')): while(have_rows('projects')): the_row(); ?>
 
                        <div class="project">

                            <div class="info">
                                <div class="headline">
                                    <h3><?php echo get_sub_field('title'); ?></h3>
                                </div>

                                <div class="copy copy-2">
                                    <?php echo get_sub_field('copy'); ?>
                                </div>

                            </div>        
                        </div>

                    <?php endwhile; endif; ?>
                </div>                
            </section>

        <?php endif; ?>

    <?php $count++; endwhile; endif; ?>

</section>