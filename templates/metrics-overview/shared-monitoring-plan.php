<?php

    $plan = get_field('shared_monitoring_plan');
    $headline = $plan['headline'];
    $copy = $plan['copy'];
    $graphic = $plan['graphic'];

?>

<section class="shared-monitoring-plan grid">

    <div class="card blue">
        <div class="card-header">
            <h3><?php echo $headline; ?></h3>
        </div>

        <div class="card-body copy copy-2 extended">
            <?php echo $copy; ?>
        </div>
    </div>

    <div class="graphic">
        <?php echo wp_get_attachment_image($graphic['ID'], 'full'); ?>
    </div>

</section>