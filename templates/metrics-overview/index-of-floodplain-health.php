<?php

    $index = get_field('index_of_floodplain_health');
    $headline = $index['headline'];
    $copy = $index['copy'];
    $graphic = $index['graphic'];

?>

<section class="index-of-floodplain-health grid">

    <div class="card green">
        <div class="card-header">
            <h3><?php echo $headline; ?></h3>
        </div>

        <div class="card-body copy copy-2 extended">
            <?php echo $copy; ?>
        </div>
    </div>

    <div class="graphic">
        <?php 
            $svg = esa_svg($graphic['url']);
            echo $svg; 
        ?>
    </div>

</section>