<?php

/*
	Template Name: Home
*/

get_header(); ?>


	<?php get_template_part('templates/home/hero'); ?>

	<?php get_template_part('templates/home/about'); ?>

	<?php get_template_part('templates/home/links'); ?>
    
<?php get_footer(); ?>