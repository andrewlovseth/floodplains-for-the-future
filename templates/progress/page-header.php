<?php

    $page_header = get_field('page_header');
    $headline = $page_header['headline'];
    $sub_headline = $page_header['sub_headline'];
    $logo = $page_header['logo'];
    $copy = $page_header['copy'];
    $color = $page_header['color'];

?>

<section class="page-header grid">
    <?php if($color): ?>
        <style type="text/css">
            .page-title {
                color: <?php echo $color; ?>;
                border-color: <?php echo $color; ?>;
            }

            .tab-list {
                border-color: <?php echo $color; ?>;
            }

            .tab-list a {
                border-color: <?php echo $color; ?>;
            }

            .tab-list a[aria-selected='true'],
            .tab-list a:hover {
                background-color: <?php echo $color; ?>;
            }

        </style>
    <?php endif; ?>

    <h1 class="page-title"><?php the_title(); ?></h1>

    <div class="headline question">
        <h2><?php echo $headline; ?></h2>
    </div>

    <div class="info">
        <div class="sub-headline question">
            <div class="logo">
                <?php echo wp_get_attachment_image($logo['ID'], 'full'); ?>
            </div>

            <h2><?php echo $sub_headline; ?></h2>
        </div>

        <div class="copy copy-2 extended">
            <?php echo $copy; ?>
        </div>
    </div>


</section>