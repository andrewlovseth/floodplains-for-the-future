<section class="tabs grid">
    
    <?php if(have_rows('tabs')): ?>
        <div class="tab-list">
            
            <?php $count = 1; while(have_rows('tabs')) : the_row(); ?>
                <?php if( get_row_layout() == 'tab' ): ?>

                    <?php
                        $title = get_sub_field('title');
                        $slug = sanitize_title_with_dashes($title);
                    ?>

                    <a href="#" <?php if($count === 1): ?>aria-selected="true" <?php endif; ?>role="tab" id="<?php echo $slug; ?>"><?php echo $title; ?></a>
                    
                <?php endif; ?>
            <?php $count++; endwhile; ?>

        </div>
    
    <?php endif; ?>

    <?php if(have_rows('tabs')): $count = 1; while(have_rows('tabs')) : the_row(); ?>
        <?php if( get_row_layout() == 'tab' ): ?>

            <?php
                $title = get_sub_field('title');
                $slug = sanitize_title_with_dashes($title);
                $copy = get_sub_field('copy');
                $results = get_sub_field('results');
            ?>

            <div class="tab-panel" role="tabpanel" aria-labelledby="<?php echo $slug; ?>"<?php if($count > 1): ?> hidden<?php endif; ?>>
                <div class="tab-grid">

                    <div class="basic-info">
                        <div class="headline">
                            <h3><?php echo $title; ?></h3>
                        </div>

                        <div class="copy copy-2 extended">
                            <?php echo $copy; ?>
                        </div>
                    </div>

                    <?php if(have_rows('vitals')): ?>
                        <div class="vitals">
                            <?php while(have_rows('vitals')): the_row(); ?>
                            
                                <div class="vital">
                                    <div class="key">
                                        <h5><?php echo get_sub_field('key'); ?>:</h5>
                                    </div>

                                    <div class="value">
                                        <?php echo get_sub_field('value'); ?>
                                    </div>                                    
                                </div>

                            <?php endwhile; ?>
                        </div>
                    <?php endif; ?>

                    <?php if($results): ?>

                        <div class="results copy copy-2 extended">
                            <?php echo $results; ?>
                        </div>

                    <?php endif; ?>

                    <?php if(have_rows('graphs')): ?>

                        <div class="graphs">
                            <?php while(have_rows('graphs')): the_row(); ?>

                                <?php
                                    $graph = get_sub_field('graph');
                                    $caption = $graph['caption'];
                                ?>
                            
                                <div class="graph">
                                    <a data-fslightbox href="<?php echo $graph['url']; ?>">
                                        <?php echo wp_get_attachment_image($graph['ID'], 'full'); ?>
                                    </a>

                                    <?php if($caption): ?>
                                        <div class="caption">
                                            <p><?php echo $caption; ?></p>
                                        </div>
                                    <?php endif; ?>
                                </div>

                            <?php endwhile; ?>
                        </div>

                    <?php endif; ?>    

                </div>
            </div>

        <?php endif; ?>
    <?php $count++; endwhile; endif; ?>

</section>