<?php

/*
	Template Name: Metrics Overview
*/

get_header(); ?>

	<?php get_template_part('template-parts/global/page-title'); ?>

   	<?php get_template_part('templates/metrics-overview/shared-monitoring-plan'); ?>

   	<?php get_template_part('templates/metrics-overview/index-of-floodplain-health'); ?>


<?php get_footer(); ?>