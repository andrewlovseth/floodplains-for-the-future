<?php

/*
	Template Name: Data Collection
*/

get_header(); ?>

	<?php get_template_part('template-parts/global/page-title'); ?>

	<?php get_template_part('templates/data-collection/intro'); ?>

	<?php get_template_part('templates/data-collection/projects'); ?>

	<?php get_template_part('templates/data-collection/request'); ?>
	
<?php get_footer(); ?>