<?php get_header(); ?>

    <?php get_template_part('templates/archive-projects/page-title'); ?>

    <?php get_template_part('templates/archive-projects/sections'); ?>
    
<?php get_footer(); ?>