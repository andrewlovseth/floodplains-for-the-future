<?php get_header(); ?>

    <?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

        <?php get_template_part('templates/single-success-stories/article'); ?>

    <?php endwhile; endif; ?>

    <?php get_template_part('templates/single-success-stories/recent'); ?>

<?php get_footer(); ?>