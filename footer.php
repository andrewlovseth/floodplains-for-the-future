	</main> <!-- .site-content -->

	<footer class="site-footer grid">
		<div class="footer-grid">
			<?php get_template_part('template-parts/footer/logo'); ?>

			<?php get_template_part('template-parts/footer/about'); ?>

			<?php get_template_part('template-parts/footer/contact'); ?>

			<?php // get_template_part('template-parts/footer/recent-projects'); ?>

			<?php // get_template_part('template-parts/footer/resources'); ?>

			<?php get_template_part('template-parts/footer/copyright'); ?>
		</div>
	</footer>

<?php wp_footer(); ?>
<script src="<?php bloginfo('template_directory'); ?>/src/js/vendor/fslightbox.js"></script>


</div> <!-- .site -->

</body>
</html>