const Content = {
    svgLinks() {
        const svgLinks = document.querySelectorAll(".wheel-graphic .internal-link");

        svgLinks.forEach((link) => {
            const url = link.dataset.url;

            link.addEventListener("click", (e) => {
                console.log(url);
                window.location.href = url;
            });
        });
    },

    tabs() {
        const tabs = document.querySelector(".tabs");
        if (tabs) {
            const tabButtons = tabs.querySelectorAll('[role="tab"]');
            const tabPanels = Array.from(tabs.querySelectorAll('[role="tabpanel"]'));

            function handleTabClick(event) {
                tabPanels.forEach((panel) => {
                    panel.hidden = true;
                });

                tabButtons.forEach((tab) => {
                    tab.setAttribute("aria-selected", false);
                });

                event.currentTarget.setAttribute("aria-selected", true);

                const { id } = event.currentTarget;

                const tabPanel = tabPanels.find((panel) => panel.getAttribute("aria-labelledby") === id);
                tabPanel.hidden = false;

                event.preventDefault();
            }

            tabButtons.forEach((button) => button.addEventListener("click", handleTabClick));
        }
    },

    glossary() {
        const items = document.querySelectorAll(".glossary-list .entry");

        items.forEach((item) => {
            const word = item.querySelector(".word");
            const description = item.querySelector(".description");

            word.addEventListener("click", (e) => {
                console.log("hi");
                item.classList.toggle("open");
            });
        });
    },

    workFilter() {
        const selectElements = document.querySelectorAll(".filter-select");

        selectElements.forEach((selectElement) => {
            selectElement.addEventListener("change", function () {
                const filterValue = this.value;

                // Synchronize all select elements
                selectElements.forEach((otherSelect) => {
                    otherSelect.value = filterValue;
                });

                // Filter items and check for visibility
                selectElements.forEach((select) => {
                    const tab = select.closest(".tab-panel");
                    const items = tab.querySelectorAll(".work-item");
                    let allHidden = true;

                    items.forEach((item) => {
                        if (filterValue === "all" || item.getAttribute("data-reach-filter") === filterValue) {
                            item.style.display = "";
                            allHidden = false; // There's at least one item visible
                        } else {
                            item.style.display = "none";
                        }
                    });

                    // Display the 'no results' message if all items are hidden
                    const noResultsMessage = tab.querySelector(".no-results");
                    if (allHidden) {
                        noResultsMessage.style.display = "block";
                    } else {
                        noResultsMessage.style.display = "none";
                    }
                });
            });
        });
    },

    init: function () {
        this.svgLinks();
        this.tabs();
        this.glossary();
        this.workFilter();
    },
};

export default Content;
