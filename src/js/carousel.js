//import Swiper from 'swiper';
//import 'swiper/css';

const Carousel = {
    swiperSlider() {
        const swiper = new Swiper('.swiper', {
            autoplay: {
                delay: 8000,
                disableOnInteraction: false
            },
            speed: 600,
            spaceBetween: 0,
            grabCursor: true,
            pauseOnMouseEnter: true,
            loop: true,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            pagination: {
                el: '.swiper-pagination',
                type: 'bullets',
                clickable: true
            },
        });


    },
    init: function() {
        this.swiperSlider();
    },
};

export default Carousel;



