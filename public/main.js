import Header from "./header.js";
import Animations from "./animations.js";
import Carousel from "./carousel.js";
import Content from "./content.js";
import Isotope from "./isotope.js";

(() => {
    Header.init();
    Animations.init();
    Carousel.init();
    Content.init();
    Isotope.init();
})();
