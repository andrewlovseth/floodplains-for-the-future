<?php get_header(); ?>

    <?php get_template_part('template-parts/global/page-title'); ?>

    <section class="project-content grid">
        <?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

            <div class="featured-image">
                <?php if(get_the_post_thumbnail()): ?>
                        <?php the_post_thumbnail('large'); ?>
                <?php else: ?>
                    <img src="<?php bloginfo('template_directory'); ?>/src/images/FPO-project.jpeg" alt="FPO" />
                <?php endif; ?>
            </div>

            <div class="copy copy-1 extended">
                <?php the_content(); ?>
            </div>

        <?php endwhile; endif; ?>

        <?php $before_after = get_field('before_and_after'); if( $before_after ): ?>
            <div class="before-and-after">
                <?php echo wp_get_attachment_image($before_after['ID'], 'full'); ?>
            </div>
        <?php endif; ?>


        <?php get_template_part('templates/single-projects/sidebar'); ?>

    </section>


<?php get_footer(); ?>