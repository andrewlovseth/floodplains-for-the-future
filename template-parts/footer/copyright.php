<?php

    $footer = get_field('footer', 'options');
    $copyright = $footer['copyright'];

?>

<div class="copyright copy copy-3">
    <p><?php echo $copyright; ?></p>
</div>