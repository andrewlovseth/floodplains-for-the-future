<?php

    $footer = get_field('footer', 'options');
    $about = $footer['about'];
    $headline = $about['headline'];
    $copy = $about['copy'];

?>

<div class="about footer-col">
    <div class="headline">
        <h4 class="footer-headline"><?php echo $headline; ?></h4>
    </div>

    <div class="copy copy-2">
        <?php echo $copy; ?>
    </div>
</div>