<?php

    $footer = get_field('footer', 'options');
    $recent_projects = $footer['recent_projects'];
    $headline = $recent_projects['headline'];

?>

<div class="recent-projects footer-col">
    <div class="headline">
        <h4 class="footer-headline"><?php echo $headline; ?></h4>
    </div>

    <div class="copy copy-2">
        <?php
            $args = array(
                'post_type' => 'projects',
                'posts_per_page' => 5
            );
            $query = new WP_Query( $args );
            if ( $query->have_posts() ) : ?>

            <ul>    
                <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                    <li>
                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    </li>
                <?php endwhile; ?>
            </ul>

        <?php endif; wp_reset_postdata(); ?>
    </div>
</div>