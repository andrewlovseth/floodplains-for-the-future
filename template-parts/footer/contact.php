<?php

    $footer = get_field('footer', 'options');
    $contact = $footer['contact'];
    $headline = $contact['headline'];
    $copy = $contact['copy'];
    $address = $contact['address'];
    $phone = $contact['phone'];
    $email = $contact['email'];

?>

<div class="contact footer-col">
    <div class="headline">
        <h4 class="footer-headline"><?php echo $headline; ?></h4>
    </div>

    <div class="copy copy-2">
        <?php echo $copy; ?>

        <div class="detail address">
            <p>
                <?php get_template_part('src/svgs/home'); ?>
                <?php echo $address; ?>
            </p>
        </div>

        <div class="detail phone">
            <p>
                <?php get_template_part('src/svgs/phone'); ?>
                <?php echo $phone; ?>
            </p>
        </div>

        <div class="detail email">
            <p>
                <?php get_template_part('src/svgs/email'); ?>
                <a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
            </p>

        </div>

    </div>
</div>