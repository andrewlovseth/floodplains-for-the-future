<?php

    $footer = get_field('footer', 'options');
    $resources = $footer['resources'];
    $headline = $resources['headline'];
    $resource_links = $resources['links'];

?>

<div class="resources footer-col">
    <div class="headline">
        <h4 class="footer-headline"><?php echo $headline; ?></h4>
    </div>

    <div class="copy copy-2">
        <?php if( $resource_links ): ?>
            <ul>
                <?php foreach( $resource_links as $resource ):  ?>

                    <?php
                        $type = get_field('type', $resource->ID);
                        if($type == 'file') {
                            $url = get_field('file', $resource->ID);
                        } elseif($type == 'link') {
                            $url = get_field('link', $resource->ID);
                        }
                    ?>

                    <li>
                        <a href="<?php echo $url; ?>" target="window"><?php echo get_the_title( $resource->ID ); ?></a>
                    </li>

                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
    </div>
</div>