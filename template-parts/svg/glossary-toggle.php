<svg width="256px" height="256px" viewBox="0 0 256 256" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="Group">
            <circle id="Oval" fill="#7C986A" cx="128" cy="128" r="128"></circle>
            <rect id="Rectangle" fill="#FFFFFF" x="116" y="44" width="24" height="168"></rect>
            <rect id="Rectangle" fill="#FFFFFF" transform="translate(128.000000, 128.000000) rotate(90.000000) translate(-128.000000, -128.000000) " x="116" y="44" width="24" height="168"></rect>
        </g>
    </g>
</svg>